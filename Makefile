all: Sisterhood.pdf clean

.PHONY: clean
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.out

Sisterhood.pdf: Sisterhood.tex
	pdflatex Sisterhood.tex
	pdflatex Sisterhood.tex
	pdflatex Sisterhood.tex

